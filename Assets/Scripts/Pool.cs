using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour{
    [SerializeField] private GameObject poolObject;
    [SerializeField] private int prewarmCount;
    [SerializeField] private Transform poolContainer;

    private List<GameObject> poolList = new List<GameObject>();

    public void InitPool(){
        for (int i = 0; i < prewarmCount; i++){
            GameObject item = Instantiate(poolObject, poolContainer);
            item.SetActive(false);
            poolList.Add(item);
        }
    }


    public GameObject GetItem(){
        foreach (GameObject item in poolList){
            if (!item.activeInHierarchy){
                return item;
            }
        }

        return AddExtraNewItem();
    }

    public void ReleaseItem(GameObject item){
        item.SetActive(false);
        ResetItem(item);
    }

    private void ResetItem(GameObject item){
        item.transform.position = Vector3.zero;
        item.transform.rotation = Quaternion.identity;
        var rb = item.GetComponent<Rigidbody>();
        if (rb) ResetRigidbody(rb);
    }

    private void ResetRigidbody(Rigidbody rb){
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }

    private GameObject AddExtraNewItem(){
        GameObject item = Instantiate(poolObject);
        item.SetActive(false);
        poolList.Add(item);
        return item;
    }
}