using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleAmmo : MonoBehaviour, IBullet{
    [SerializeField] private float damage = 35f;
    [SerializeField] private float projectileSpeed = 1f;
    [SerializeField] private LayerMask collisionLayerMask;

    private Pool projectilePool;
    
    public void SetPool(Pool pool){
        projectilePool = pool;
    }

    public void Shoot(Transform target, Vector3 firePoint){
        GameObject missile = projectilePool.GetItem();
        missile.transform.position = firePoint;
        missile.transform.LookAt(target);
        var projectile = missile.GetComponent<Projectile>();
        projectile.SetDamageAmount(damage);
        projectile.SetCollisionMask(collisionLayerMask);
        projectile.SetPool(projectilePool);
        missile.SetActive(true);
        var rb = missile.GetComponent<Rigidbody>();
        rb.AddForce(missile.transform.forward * projectileSpeed);
    }
}