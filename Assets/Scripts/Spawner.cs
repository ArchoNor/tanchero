using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class Spawner : MonoBehaviour{
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private Transform playerStart;
    [SerializeField] private ButtonAmmoSwitch buttonAmmoSwitch;
    [SerializeField] private List<Transform> enemySpawnPoints;
    [SerializeField] private int startEnemyCount = 5;
    [SerializeField] private float enemySpawnDelay = 10f;
    [SerializeField] private Pool tanksPool;
    [SerializeField] private Pool projectilePool;

    private GameObject player;
    private TargetSelector targetSelector;
    private WaitForSeconds spawnDelay;

    private void Start(){
        targetSelector = new TargetSelector();
        tanksPool.InitPool();
        projectilePool.InitPool();
        player = Instantiate(playerPrefab, playerStart.position, Quaternion.identity);
        player.GetComponent<Player>().Init(buttonAmmoSwitch, targetSelector);
        player.GetComponentInChildren<SingleAmmo>().SetPool(projectilePool);
        for (int i = 0; i < startEnemyCount; i++){
            SpawnEnemy();
        }

        spawnDelay = new WaitForSeconds(enemySpawnDelay);
        StartCoroutine(PeriodicalSpawn());
    }

    private void SpawnEnemy(){
        GameObject enemy = tanksPool.GetItem();
        int randomIndex = Random.Range(0, enemySpawnPoints.Count);
        enemy.transform.position = enemySpawnPoints[randomIndex].position;
        targetSelector.AddTarget(enemy);
        enemy.GetComponent<EnemyAI>().Init(player.transform, enemySpawnPoints, randomIndex);
        enemy.GetComponent<Health>().TankDestroyed += OnTankDestroy;
        enemy.GetComponentInChildren<SingleAmmo>().SetPool(projectilePool);
        enemy.SetActive(true);
    }

    private IEnumerator PeriodicalSpawn(){
        while (gameObject.activeInHierarchy){
            yield return spawnDelay;
            SpawnEnemy();
        }
    }

    private void OnTankDestroy(GameObject item){
        item.GetComponent<Health>().TankDestroyed -= OnTankDestroy;
        targetSelector.RemoveTarget(item);
        tanksPool.ReleaseItem(item);
    }
}