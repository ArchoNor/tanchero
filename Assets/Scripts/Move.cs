using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour{
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody rb;

    private Vector3 currentDirection;

    public void SetDirection(Vector3 direction){
        currentDirection = direction;
    }

    private void FixedUpdate(){
        if (currentDirection == Vector3.zero){
            Stop();
        }
        else{
            BodyRotate();
            MoveInDirection();
        }
    }

    private void Stop(){
        rb.velocity = Vector3.zero;
    }

    private void BodyRotate(){
        Quaternion newRotation = Quaternion.LookRotation(currentDirection,Vector3.up);
        transform.localRotation = newRotation;
    }

    private void MoveInDirection(){
        Vector3 velocity = new Vector3(currentDirection.x, 0f, currentDirection.z).normalized * speed;
        rb.velocity = velocity;
    }
}