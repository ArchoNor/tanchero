using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BuckshotAmmo : MonoBehaviour, IBullet{
    [SerializeField] private float damage = 10f;
    [SerializeField] private int buckshotPiecesNumber = 5;
    [SerializeField] private float minSpread = -6f;
    [SerializeField] private float maxSpread = 6f;
    [SerializeField] private float maxRange = 3f;
    [SerializeField] private LayerMask enemyLayerMask;
    [SerializeField] private ParticleSystem muzzleFlash;

    public void Shoot(Transform target, Vector3 firePoint){
        Vector3 direction = target.position - firePoint;
        muzzleFlash.Play();
        for (int i = 0; i < buckshotPiecesNumber; i++){
            Vector3 randomizedDirection = Quaternion.Euler(0f, Random.Range(minSpread, maxSpread), 0f) * direction;
            var ray = new Ray(firePoint, randomizedDirection);
            Debug.DrawRay(firePoint, randomizedDirection, Color.yellow, 1f);
            if (!Physics.Raycast(ray, out RaycastHit hit, maxRange, enemyLayerMask)) continue;
            var health = hit.collider.GetComponent<Health>();
            if (health) health.TakeDamage(damage);
        }
    }
}