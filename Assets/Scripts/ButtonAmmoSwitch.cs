using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonAmmoSwitch : MonoBehaviour{
    public delegate void Message(bool value);

    public event Message AmmoSwitchButtonPressed;

    [SerializeField] private Image buttonImage;
    [SerializeField] private Sprite singleShotImage;
    [SerializeField] private Sprite buckshotImage;
    private bool isBuckshot;

    public void SwitchAmmo(){
        isBuckshot = !isBuckshot;
        buttonImage.sprite = isBuckshot ? buckshotImage : singleShotImage;
        AmmoSwitchButtonPressed?.Invoke(isBuckshot);
    }
}