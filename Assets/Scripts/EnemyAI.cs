using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyAI : MonoBehaviour{
    [SerializeField] private float agroRange = 1f;
    [SerializeField] private Transform currentTarget;
    [SerializeField] private Cannon cannon;
    [SerializeField] private float waypointSwitchProximity = 0.1f;
    [SerializeField] private float recalculatePathDelay = 0.2f;
    [SerializeField] private Move move;

    private NavMeshAgent navMeshAgent;
    private List<Vector3> waypoints = new List<Vector3>();
    private List<Transform> patrolWaypoints;
    private int currentWaypointIndex;
    private bool isChasingPlayer;
    private WaitForSeconds recalculateDelay;

    private void Awake(){
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public void Init(Transform target, List<Transform> startWaypoints, int waypointIndex){
        currentTarget = target;
        cannon.SetTarget(target);
        patrolWaypoints = startWaypoints;
        currentWaypointIndex = waypointIndex;
        foreach (Transform waypoint in patrolWaypoints){
            waypoints.Add(waypoint.position);
        }
    }

    private void Update(){
        if (!isChasingPlayer && IsTargetInAgroRange()){
            isChasingPlayer = true;
            recalculateDelay = new WaitForSeconds(recalculatePathDelay);
            StartCoroutine(RecalculatePath());
        }

        CheckWaypoint();
        Vector3 direction = waypoints[currentWaypointIndex] - transform.position;
        move.SetDirection(direction);
    }

    private bool IsTargetInAgroRange(){
        if (!currentTarget) return false;
        float distanceToTarget = Vector3.Distance(transform.position, currentTarget.position);
        return distanceToTarget <= agroRange;
    }

    private void CheckWaypoint(){
        float distanceToWaypoint = Vector3.Distance(transform.position, waypoints[currentWaypointIndex]);
        if (distanceToWaypoint > waypointSwitchProximity) return;
        NextWaypoint();
    }

    private void NextWaypoint(){
        if (currentWaypointIndex == waypoints.Count - 1){
            currentWaypointIndex = 0;
        }
        else{
            currentWaypointIndex++;
        }
    }

    private IEnumerator RecalculatePath(){
        var path = new NavMeshPath();
        while (gameObject.activeInHierarchy){
            navMeshAgent.CalculatePath(currentTarget.position, path);
            waypoints = path.corners.ToList();
            currentWaypointIndex = 0;
            yield return recalculateDelay;
        }
    }
}