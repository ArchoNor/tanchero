using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour{
    [SerializeField] private float delayBetweenShots = 1f;
    [SerializeField] private float fireRange = 1f;
    [SerializeField] private Transform firePoint;
    [SerializeField] private SingleAmmo singleAmmo;
    [SerializeField] private BuckshotAmmo buckshotAmmo;

    private Transform currentTarget;
    private bool isReadyToShoot = true;
    private IBullet currentBullet;

    private void Start(){
        currentBullet = singleAmmo;
    }

    public void SetTarget(Transform target){
        currentTarget = target;
    }

    private void Update(){
        if (!currentTarget || !IsTargetInFireRange()) return;
        transform.LookAt(currentTarget);
        if (!isReadyToShoot) return;
        currentBullet.Shoot(currentTarget, firePoint.position);
        StartCoroutine(Countdown());
    }

    private bool IsTargetInFireRange(){
        return Vector3.Distance(transform.position, currentTarget.position) <= fireRange;
    }

    public void SwitchBullet(bool isBuckshot){
        currentBullet = isBuckshot ? buckshotAmmo : singleAmmo;
    }

    private IEnumerator Countdown(){
        isReadyToShoot = false;
        yield return new WaitForSeconds(delayBetweenShots);
        isReadyToShoot = true;
    }
}