using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour{
    [SerializeField] private Move move;
    [SerializeField] private Camera playerCamera;
    [SerializeField] private Cannon cannon;
    [SerializeField] private float checkNearestTargetDelay = 0.2f;
    private WaitForSeconds checkDelay;
    private ButtonAmmoSwitch buttonAmmoSwitch;
    private TargetSelector targetSelector;
    private float nearClipPlane;
    private Vector3 startPoint;
    private Vector3 endPoint;
    private Vector3 startPosition;
    private Vector3 endPosition;
    private bool isMouseButtonDown;
    private bool isInit;


    public void Init(ButtonAmmoSwitch button, TargetSelector selector){
        buttonAmmoSwitch = button;
        targetSelector = selector;
        isInit = true;
        OnEnable();
    }

    private void Start(){
        nearClipPlane = playerCamera.nearClipPlane;
        checkDelay = new WaitForSeconds(checkNearestTargetDelay);
        StartCoroutine(ChooseNearestTarget());
    }

    private void OnEnable(){
        if (!isInit) return;
        buttonAmmoSwitch.AmmoSwitchButtonPressed += OnAmmoSwitch;
    }

    private void OnDisable(){
        buttonAmmoSwitch.AmmoSwitchButtonPressed -= OnAmmoSwitch;
    }

    private void OnAmmoSwitch(bool isBuckshot){
        cannon.SwitchBullet(isBuckshot);
    }

    private void Update(){
        if (Input.GetMouseButtonDown(0)){
            isMouseButtonDown = true;
            startPoint = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0)) isMouseButtonDown = false;
        if (Input.GetMouseButton(0)) endPoint = Input.mousePosition;

        var startPointOnNearClipPanel = new Vector3(startPoint.x, startPoint.y, nearClipPlane);
        var endPointOnNearClipPanel = new Vector3(endPoint.x, endPoint.y, nearClipPlane);
        startPosition = playerCamera.ScreenToWorldPoint(startPointOnNearClipPanel);
        endPosition = playerCamera.ScreenToWorldPoint(endPointOnNearClipPanel);


        Vector3 direction = endPosition - startPosition;

        move.SetDirection(isMouseButtonDown ? direction : Vector3.zero);
    }

    private IEnumerator ChooseNearestTarget(){
        while (gameObject.activeInHierarchy){
            if(targetSelector.IsThereEnemy()){
                cannon.SetTarget(targetSelector.GetNearestTargetTransform(transform.position));
            }
            yield return checkDelay;
        }
    }
}