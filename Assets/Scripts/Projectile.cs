using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour{
    [SerializeField] private float damage = 20f;
    private LayerMask collisionLayerMask;
    private Pool projectilePool;

    public void SetPool(Pool pool){
        projectilePool = pool;
    }

    public void SetDamageAmount(float damageAmount){
        damage = damageAmount;
    }

    public void SetCollisionMask(LayerMask mask){
        collisionLayerMask = mask;
    }

    private void OnTriggerEnter(Collider other){
        if ((collisionLayerMask.value & 1 << other.gameObject.layer) == 0) return;
        var health = other.GetComponent<Health>();
        if (health) health.TakeDamage(damage);
        projectilePool.ReleaseItem(gameObject);
    }
}