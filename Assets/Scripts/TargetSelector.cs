using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSelector{
    private List<GameObject> targetsList = new List<GameObject>();

    public void AddTarget(GameObject target){
        targetsList.Add(target);
    }

    public void RemoveTarget(GameObject target){
        targetsList.Remove(target);
    }

    public bool IsThereEnemy(){
        return targetsList.Count > 0;
    }

    public Transform GetNearestTargetTransform(Vector3 position){
        float minDistance = Vector3.Distance(position, targetsList[0].transform.position);
        Transform selectedTargetTransform = targetsList[0].transform;
        for (int i = 1; i < targetsList.Count; i++){
            float distance = Vector3.Distance(position, targetsList[i].transform.position);
            if (distance > minDistance) continue;
            minDistance = distance;
            selectedTargetTransform = targetsList[i].transform;
        }

        return selectedTargetTransform;
    }
}
