using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour{
    public delegate void Message(GameObject item);

    public event Message TankDestroyed;

    [SerializeField] private float startHitPoints = 100f;
    [SerializeField] private float collisionDamageCoefficient = 0.01f;
    [SerializeField] private float spawnInvincibleTime = 0.1f;
    private float hitPoints = 100f;
    private bool isInvincible = true;

    private void OnEnable(){
        hitPoints = startHitPoints;
        StartCoroutine(Delay());
    }

    public void TakeDamage(float amount){
        if (isInvincible) return;
        hitPoints -= amount;
        if (hitPoints <= 0){
            TankDestroyed?.Invoke(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision){
        if (isInvincible) return;
        Vector3 collisionForce = collision.impulse / Time.fixedDeltaTime;
        TakeDamage(collisionForce.sqrMagnitude * collisionDamageCoefficient);
    }

    private IEnumerator Delay(){
        yield return new WaitForSeconds(spawnInvincibleTime);
        isInvincible = false;
    }
}