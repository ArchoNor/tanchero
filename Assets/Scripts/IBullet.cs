using UnityEngine;
public interface IBullet{
    public void Shoot(Transform target, Vector3 firePoint);
}
